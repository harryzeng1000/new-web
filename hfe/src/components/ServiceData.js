import { useState } from 'react';
import "./ServiceStyles.css";

function ServiceData(props) {
  const [showVideo, setShowVideo] = useState(false);

  return (
    <div className="s-card">
      <div className="s-image" onClick={() => setShowVideo(true)}>
        {showVideo ? (
          <video width="100%" controls autoPlay onEnded={() => setShowVideo(false)}>
            <source src={props.videoURL} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
        ) : (
          <img src={props.image} alt="service" />
        )}
      </div>
      <h3>{props.heading}</h3>
      
    </div>
  );
}

export default ServiceData;
