import "./HomeHeroStyles.css";

function HomeHero(props) {
  return (
    <>
      <div className={props.cName}>
      <img src={props.heroImg} alt="heroImg" />
        <video className="heroVideo" src={props.video} autoPlay muted loop alt="heroVideo"/>
        <div className="hero-text">
          <h1>{props.title}</h1>
          <p>{props.text}</p>
          <a className={`first-btn ${props.btnClass}`} href={props.url}>
            {props.buttonText}
          </a>
          <a className={`second-btn ${props.secondBtnClass}`} href={props.secondUrl}>
            {props.secondButtonText}
          </a>
        </div>
      </div>
    </>
  );
}

export default HomeHero;
