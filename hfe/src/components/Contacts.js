import "./ContactFormStyles.css";
import Contacts from "./ContactData";
import p1 from "../assets/contact/cindy.jpg";
import p2 from "../assets/contact/joie.jpg";
import p3 from "../assets/contact/harry.jpg";




function ContactForm () {
  return (
    <div className="Cont">
      <h1>Our Staff</h1>
      <p>Elegance in Every Endeavor</p>
      <div className="Itemcard">
        <Contacts
          image={p1}
          name="Cindy"
          title="North American Sales Manager"
          email="Sales3@royalsaunas.com"
          phone=""
        />
        <Contacts
        image={p2}
        name="Joie"
        title="Sales Representative"
        email="Sales8@royalsaunas.com"
        phone=""
        />
        <Contacts
        image={p3}
        name="Harry"
        title="Sales Representative"
        email="Royalsaunas3@gmail.com"
        phone=""
        />
      </div>
    </div>
  );
}

export default ContactForm;
