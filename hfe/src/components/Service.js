import "./ServiceStyles.css";
import ServiceData from "./ServiceData";
import Sp1 from "../assets/customerservice/1.jpg";
import Sp2 from "../assets/customerservice/2.jpg";
import Sp3 from "../assets/customerservice/3.jpg";
import Service1 from "../assets/customerservice/HeaterReplace.mp4";
import Service2 from "../assets/customerservice/Controlbox.mp4";
import Service3 from "../assets/customerservice/Controlpanel.mp4";

function Service() {
  return (
    <div className="service">
      <h1>FAQ</h1>
      <p>You can discover our saunas room using this link.</p>
      <div className="servicecard">
        <ServiceData
          image={Sp1}
          heading="Heater Replacement"
          videoURL={Service1}  // Add your video link here
        />
        <ServiceData
          image={Sp2}
          heading="Control Box Replacement"
          videoURL={Service2}  // Add your video link here
          url="https://www.wayfair.com/outdoor/pdp/royal-saunas-hongyuan-hongyuan-1-person-indoor-bluetooth-compatible-low-emf-far-infrared-in-okoume-hemlock-rshy1017.html"
        />
        <ServiceData
          image={Sp3}
          heading="Control Panel Replacement"
          text=""
          videoURL={Service3}  // Add your video link here
          url="https://www.amazon.com/s?i=merchant-items&me=A15Y5R1CE0IPTP"
        />
      </div>
    </div>
  );
}

export default Service;
